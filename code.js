let currentNumber = "";
let previousNumber = "";
let operator = "";
let result = 0;
//'''


function updateDisplay(number) {
    const display = document.getElementById("box");
    display.textContent = number;
}

function appendNumber(number) {
    currentNumber += number;
    updateDisplay(currentNumber);
}

function calculate() {
    const num1 = parseFloat(previousNumber);
    const num2 = parseFloat(currentNumber);
    let calculation;

    if (operator === "+") {
        calculation = num1 + num2;
    } else if (operator === "-") {
        calculation = num1 - num2;
    } else if (operator === "*") {
        calculation = num1 * num2;
    } else if (operator === "/") {
        calculation = num1 / num2;
    }

    return calculation;
}

function calculate_percentage() {
    currentNumber = parseFloat(currentNumber) / 100;
    updateDisplay(currentNumber);
}

function clear_entry() {
    currentNumber = "";
    updateDisplay(0);
}

function button_clear() {
    currentNumber = "";
    previousNumber = "";
    operator = "";
    updateDisplay(0);
}

function backspace_remove() {
    currentNumber = currentNumber.slice(0, -1);
    updateDisplay(currentNumber);
}

function division_one() {
    currentNumber = 1 / parseFloat(currentNumber);
    updateDisplay(currentNumber);
}

function power_of() {
    currentNumber = parseFloat(currentNumber) ** 2;
    updateDisplay(currentNumber);
}

function square_root() {
    currentNumber = Math.sqrt(parseFloat(currentNumber));
    updateDisplay(currentNumber);
}

function button_operator(op) {
    if (operator !== "" && currentNumber !== "") {
        const calculation = calculate();
        previousNumber = calculation.toString();
        updateDisplay(calculation);
        currentNumber = "";
    }
    operator = op;
}

function button_number(number) {
    if (operator === "=") {
        currentNumber = "";
        operator = "";
    }
    appendNumber(number);
}


function resetCalculator() {
    currentNumber = "";
    previousNumber = "";
    operator = "";
}


function button_equal() {
    if (operator !== "" && currentNumber !== "") {
        let calculation;
        if (operator === "=") {
            calculation = result;
        } else {
            calculation = calculate();
            result = calculation;
        }
        resetCalculator();
        currentNumber = calculation.toString();
        updateDisplay(calculation);
    }
}


window.addEventListener("DOMContentLoaded", () => {
    updateDisplay(currentNumber);
});
